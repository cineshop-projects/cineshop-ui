import React, { useEffect, useState } from 'react'

import { makeContext } from '../makeContext'
import { useGetCart } from '../../api/cineshop_api'
import { useAuth } from '../auth/AuthContext'

interface ISharedData {
  filter: string
  count: number
}

const localStorageKey = 'cineshop.count'

interface SharingContextType {
  sharedData: ISharedData
  onUpdate: (filter?: string) => void
}

const [useCustomContext, CustomContextProvider] = makeContext<SharingContextType>()

export const SharingProvider: React.FC = ({ children }) => {
  const { authData } = useAuth()
  const { data: cart, refetch } = useGetCart({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })
  const [filter, setFilter] = useState<string>('')
  const [count, setCount] = useState<number>(0)
  const [sharedData, setSharedData] = useState<ISharedData>({
    filter: '',
    count: 0,
  })
  const [toggled, setToggled] = useState(false)

  const onUpdate = (filter?: string) => {
    if (filter !== undefined) {
      setFilter(filter)
    }
    refetch()
      .then(_ => {})
      .catch(() => {})
  }

  useEffect(() => {
    const data = localStorage.getItem(localStorageKey)
    if (data !== null) {
      setCount(+data)
    }
  }, [])

  useEffect(() => {
    if (authData.initialized && !toggled) {
      setToggled(true)
      refetch()
        .then(() => {})
        .catch(() => {})
    }
  }, [authData, refetch, toggled])

  useEffect(() => {
    if (!authData.initialized) {
      return
    }
    let count = 0
    if (cart && cart.items) {
      count = cart.items.length
    }
    localStorage.setItem(localStorageKey, '' + count)
    setCount(count)
  }, [authData, cart])

  useEffect(() => {
    setSharedData({ filter: filter, count: count })
  }, [count, filter])

  return (
    <CustomContextProvider value={{ sharedData: sharedData, onUpdate: onUpdate }}>
      {children}
    </CustomContextProvider>
  )
}

export const useShared = useCustomContext

import { createContext, useContext } from 'react'

export function makeContext<ContextType>() {
  const customContext = createContext<ContextType | undefined>(undefined)

  function useCustomContext() {
    const ctx = useContext(customContext)
    if (!ctx) {
      throw new Error('useCustomContext must be used inside a Provider')
    }
    return ctx
  }

  return [useCustomContext, customContext.Provider] as const
}

import React, { useEffect, useState } from 'react'

import { makeContext } from '../makeContext'
import { SignInResponse } from '../../api/cineshop_api'

interface UserDetails {
  firstName: string
  lastName: string
  email: string
}

interface IAuthData {
  authenticated: boolean
  userDetails: UserDetails
  accessToken: string
  initialized: boolean
}

function newAuthData() {
  return {
    authenticated: false,
    userDetails: {
      firstName: '',
      lastName: '',
      email: '',
    },
    accessToken: '',
    initialized: false,
  }
}

const localStorageKey = 'cineshop.authData'

interface AuthContextType {
  authData: IAuthData
  onLogin: (data: SignInResponse) => void
  onLogout: () => void
}

const [useCustomContext, CustomContextProvider] = makeContext<AuthContextType>()

export const AuthProvider: React.FC = ({ children }) => {
  const [authData, setAuthData] = useState<IAuthData>(newAuthData())

  useEffect(() => {
    const data = localStorage.getItem(localStorageKey)
    if (data !== null) {
      setAuthData(JSON.parse(data) as IAuthData)
    } else {
      const data = newAuthData()
      data.initialized = true
      setAuthData(data)
    }
  }, [])

  const onLogin = (resp: SignInResponse) => {
    const data = {
      authenticated: true,
      userDetails: {
        firstName: resp.firstName,
        lastName: resp.lastName,
        email: resp.email,
      },
      accessToken: 'Bearer ' + resp.accessToken,
      initialized: true,
    }
    localStorage.setItem(localStorageKey, JSON.stringify(data))
    setAuthData(data)
  }

  const onLogout = () => {
    localStorage.removeItem(localStorageKey)
    const data = newAuthData()
    data.initialized = true
    setAuthData(data)
  }

  return (
    <CustomContextProvider
      value={{ authData: authData, onLogin: onLogin, onLogout: onLogout }}
    >
      {children}
    </CustomContextProvider>
  )
}

export const useAuth = useCustomContext

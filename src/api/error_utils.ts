import { GetDataError } from 'restful-react'

import { Error as ApiError } from './cineshop_api'

export function isDataError(error: any): error is GetDataError<ApiError> {
  return (
    typeof error.message === 'string' &&
    (typeof error.data === 'string' || isApiError(error.data))
  )
}

export function isApiError(error: any): error is ApiError {
  return (
    typeof error.code === 'number' &&
    typeof error.message === 'string' &&
    typeof error.timestamp === 'string'
  )
}

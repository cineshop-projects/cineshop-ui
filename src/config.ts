const dev = {
  apiBase: 'http://localhost:8080/v1',
}

const prod = {
  apiBase: '/v1',
}

const config = process.env.REACT_APP_STAGE === 'production' ? prod : dev

export default {
  ...config,
}

export function getImage(productId: number) {
  switch (productId) {
    case 0:
      return '/static/images/shelley-pauls-I58f47LRQYM-unsplash.jpg'
    case 1:
      return '/static/images/ioana-cristiana-0WW38q7lGZA-unsplash.jpg'
    case 2:
      return '/static/images/jonathan-mast-RW6Wz9QaoKk-unsplash.jpg'
    case 3:
      return '/static/images/james-yarema-xTEPNH-CmKo-unsplash.jpg'
    case 4:
      return '/static/images/vitamines-1327921-1279x911.jpg'
    default:
      return '/static/images/madina-sidarto-5RoEMXvHWvg-unsplash.jpg'
  }
}

import React from 'react'

import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import { amber, cyan } from '@material-ui/core/colors'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import { RestfulProvider } from 'restful-react'
import { ToastContainer } from 'react-toastify'

import Auth from './features/auth/Auth'
import AppToolbar from './features/app-toolbar/AppToolbar'
import { AuthProvider, useAuth } from './providers/auth/AuthContext'
import Products from './features/products/Products'

import 'react-toastify/dist/ReactToastify.min.css'
import './App.css'
import { SharingProvider } from './providers/sharing/SharingContext'
import Cart from './features/cart/Cart'
import Checkout from './features/checkout/Checkout'
import PaymentConfirmation from './features/payment/PaymentConfirmation'
import config from './config'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: cyan[700],
    },
    secondary: {
      main: amber[900],
    },
  },
})

function App() {
  return (
    <ThemeProvider theme={theme}>
      <AuthProvider>
        <RestfulLayer />
      </AuthProvider>
    </ThemeProvider>
  )
}

export default App

function RestfulLayer() {
  const { authData, onLogout } = useAuth()

  return (
    <RestfulProvider
      base={config.apiBase}
      onError={({ status }) => {
        if (authData.initialized && status && (status === 401 || status === 403)) {
          onLogout()
        }
      }}
    >
      <SharingProvider>
        <BrowserRouter>
          <Content />
        </BrowserRouter>
      </SharingProvider>
    </RestfulProvider>
  )
}

function Content() {
  const { authData } = useAuth()

  return (
    <>
      {authData.authenticated ? <AppToolbar /> : null}
      <AppRouter />
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={true}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover
      />
    </>
  )
}

function AppRouter() {
  const { authData } = useAuth()

  return (
    <Switch>
      <Route path={['/login', '/register']} component={Auth} />
      {!authData.initialized ? null : authData.authenticated ? (
        <>
          <Route exact path={'/products'} component={Products} />
          <Route exact path={'/cart'} component={Cart} />
          <Route exact path={'/checkout'} component={Checkout} />
          <Route exact path={'/payment'} component={PaymentConfirmation} />
          <Route exact path={'/'}>
            <Redirect to={'/products'} />
          </Route>
        </>
      ) : (
        <Route path={'/'}>
          <Redirect to={'/login'} />
        </Route>
      )}
    </Switch>
  )
}

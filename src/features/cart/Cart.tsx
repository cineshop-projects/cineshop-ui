import React, { useEffect, useState } from 'react'

import { Box, Button, CardActions, Grid, Paper, Typography } from '@material-ui/core'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'

import {
  useApplyCoupon,
  useDeleteCart,
  useDeleteCartItem,
  useGetCart,
} from '../../api/cineshop_api'
import { useAuth } from '../../providers/auth/AuthContext'
import { useShared } from '../../providers/sharing/SharingContext'
import useStyles from './styles'
import CartContent from './CartContent'
import CouponContent from './CouponContent'
import Summary from './Summary'

export default function Cart() {
  const { authData } = useAuth()
  const { onUpdate } = useShared()
  const { data: cart, refetch } = useGetCart({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })
  const { mutate: clearCart } = useDeleteCart({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })
  const { mutate: deleteItem } = useDeleteCartItem({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })
  const { mutate: applyCoupon } = useApplyCoupon({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })
  const [refresh, setRefresh] = useState(false)

  const classes = useStyles()

  useEffect(() => {
    if (refresh) {
      refetch()
        .then(() => {})
        .catch(() => {})
      onUpdate()
      setRefresh(false)
    }
  }, [refresh, refetch, onUpdate])

  const onClear = () => {
    // TODO - add confirmation dialog
    clearCart()
      .then(() => {
        setRefresh(true)
      })
      .catch(() => {})
  }

  const onDelete = (itemId: number) => {
    // TODO - add confirmation dialog
    deleteItem('' + itemId)
      .then(() => {
        setRefresh(true)
      })
      .catch(() => {})
  }

  const onQuantityUpdate = () => {
    setRefresh(true)
  }

  const onApplyCoupon = (
    couponCode: string,
    onSuccess: () => void,
    onError: () => void
  ) => {
    applyCoupon({ code: couponCode })
      .then(() => {
        onSuccess()
        setRefresh(true)
      })
      .catch(() => {
        onError()
        setRefresh(true)
      })
  }

  const content = (
    <Paper variant="outlined" className={classes.paper}>
      <Title>Shopping Cart</Title>
      <CartContent cart={cart} onDelete={onDelete} onQuantityUpdate={onQuantityUpdate} />
      <CardActions className={classes.action}>
        <Grid container justify={'flex-end'}>
          <Button
            variant={'contained'}
            startIcon={<DeleteForeverIcon />}
            color={'secondary'}
            disabled={!cart || !cart.items || cart.items.length === 0}
            onClick={onClear}
          >
            <Box pt={0.5}>Clear Cart</Box>
          </Button>
        </Grid>
      </CardActions>
    </Paper>
  )

  const coupon = (
    <Paper variant="outlined" className={classes.paper}>
      <Title>Apply coupon</Title>
      <CouponContent cart={cart} onApplyCoupon={onApplyCoupon} />
    </Paper>
  )

  const summary = (
    <Paper variant="outlined" className={classes.paper}>
      <Title>Checkout</Title>
      <Summary cart={cart} />
    </Paper>
  )

  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item sm={12} md={12} lg={7} xl={8}>
          {content}
        </Grid>
        <Grid item sm={12} md={12} lg={5} xl={4}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              {coupon}
            </Grid>
            <Grid item xs={12}>
              {summary}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}

const Title: React.FC = ({ children }) => {
  return (
    <Typography gutterBottom variant="h5" component="h3">
      <Box fontWeight={800}>{children}</Box>
    </Typography>
  )
}

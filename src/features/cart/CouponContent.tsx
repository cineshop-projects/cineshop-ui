import React from 'react'

import { Button } from '@material-ui/core'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'

import { Cart as CartModel } from '../../api/cineshop_api'
import FormTextField from '../form-text-field/FormTextField'

interface CouponContentProps {
  cart: CartModel | null
  onApplyCoupon: (couponCode: string, onSuccess: () => void, onError: () => void) => void
}

export default function CouponContent(props: CouponContentProps) {
  const { cart, onApplyCoupon } = props

  const cartCoupon = cart ? cart.couponCode : undefined

  const form = (
    <Formik
      initialValues={{ coupon: cartCoupon ? cartCoupon : '' }}
      enableReinitialize
      onSubmit={(values, { setSubmitting, resetForm }) => {
        setSubmitting(true)
        onApplyCoupon(
          values.coupon,
          () => {
            setSubmitting(false)
          },
          () => {
            setSubmitting(false)
          }
        )
        resetForm({ values: { coupon: '' } })
      }}
      validationSchema={Yup.object().shape({
        coupon: Yup.string(),
      })}
    >
      {props => {
        const { isSubmitting, values } = props
        return (
          <Form>
            <FormTextField
              id="coupon"
              name="coupon"
              label="Coupon code"
              disabled={!!cartCoupon}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              disabled={
                !!cartCoupon ||
                isSubmitting ||
                !values.coupon ||
                values.coupon.length === 0
              }
            >
              Apply Coupon
            </Button>
          </Form>
        )
      }}
    </Formik>
  )

  return <>{form}</>
}

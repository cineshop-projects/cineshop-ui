import React, { useEffect, useState } from 'react'

import { Button, Grid } from '@material-ui/core'

import { Cart as CartModel } from '../../api/cineshop_api'
import useStyles from './styles'
import { useHistory } from 'react-router-dom'

export default function Summary(props: { cart: CartModel | null }) {
  const { cart } = props
  const [redirect, setRedirect] = useState(false)
  const history = useHistory()

  const classes = useStyles()

  useEffect(() => {
    if (redirect) {
      history.push('/checkout')
      setRedirect(false)
    }
  }, [history, redirect])

  const titleColumnSize = 5
  const valueColumnSize = 7

  let subtotal = '-'
  let discount = '-'
  let total = '-'
  let shipping = '-'

  if (cart && cart.total > 0 && cart.saleTotal > 0) {
    subtotal = cart.total.toFixed(2) + ' €'
    total = cart.saleTotal.toFixed(2) + ' €'
    discount = (cart.total - cart.saleTotal).toFixed(2) + ' €'
    shipping = 'FREE'
  }

  const Title: React.FC = ({ children }) => {
    return (
      <Grid item xs={titleColumnSize} className={classes.summaryTitle}>
        {children}
      </Grid>
    )
  }

  const summary = (
    <Grid container spacing={2}>
      <Title>Subtotal</Title>
      <Grid item xs={valueColumnSize}>
        {subtotal}
      </Grid>
      <Title>Shipping Fee</Title>
      <Grid item xs={valueColumnSize}>
        {shipping}
      </Grid>
      <Title>Discount</Title>
      <Grid item xs={valueColumnSize}>
        {discount}
      </Grid>
      <Grid item xs={12}>
        <hr />
      </Grid>
      <Grid item xs={titleColumnSize} className={classes.total}>
        Total to Pay
      </Grid>
      <Grid item xs={valueColumnSize}>
        {total}
      </Grid>
    </Grid>
  )

  return (
    <Grid container spacing={5}>
      <Grid item xs={12}>
        {summary}
      </Grid>
      <Grid item xs={12}>
        <Button
          fullWidth
          variant="contained"
          color="primary"
          disabled={!cart || cart.total === 0 || cart.saleTotal === 0}
          onClick={() => {
            setRedirect(true)
          }}
        >
          Checkout
        </Button>
      </Grid>
    </Grid>
  )
}

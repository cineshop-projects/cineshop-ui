import React, { useEffect, useState } from 'react'

import {
  Box,
  CardActionArea,
  CardContent,
  CardMedia,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'

import { Cart as CartModel } from '../../api/cineshop_api'
import useStyles from './styles'
import ItemQuantity from './ItemQuantity'
import { getImage } from '../../config'

interface CartContentProps {
  cart: CartModel | null
  onDelete: (itemId: number) => void
  onQuantityUpdate: () => void
}

export default function CartContent(props: CartContentProps) {
  const { cart, onDelete, onQuantityUpdate } = props
  const [toDelete, setToDelete] = useState<number>()

  const classes = useStyles()

  useEffect(() => {
    if (toDelete) {
      setToDelete(undefined)
      onDelete(toDelete)
    }
  }, [onDelete, toDelete])

  const columnSizes = (
    <colgroup>
      <col style={{ width: '40%' }} />
      <col style={{ width: '25%' }} />
      <col style={{ width: '15%' }} />
      <col style={{ width: '15%' }} />
      <col style={{ width: '5%' }} />
    </colgroup>
  )

  const tableHead = (
    <TableHead>
      <TableRow>
        {['Item', 'Quantity', 'Price', 'Total', ''].map((header, idx) => (
          <TableCell key={idx} align={'center'}>
            <Box fontWeight={900} fontSize={16}>
              {header}
            </Box>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )

  const tableBody = (
    <TableBody>
      {cart && cart.items
        ? cart.items.map((item, idx) => {
            return (
              <TableRow key={idx}>
                <TableCell>
                  <CardActionArea>
                    <CardMedia
                      className={classes.media}
                      image={item.image ? item.image : getImage(item.id)}
                      title={item.label}
                    />
                    <CardContent className={classes.description}>
                      <Box textAlign={'center'}>{item.label}</Box>
                    </CardContent>
                  </CardActionArea>
                </TableCell>
                <CartTableCell>
                  <ItemQuantity item={item} onQuantityUpdate={onQuantityUpdate} />
                </CartTableCell>
                <CartTableCell>{item.price.toFixed(2) + ' €'}</CartTableCell>
                <CartTableCell>
                  {(item.price * item.quantity).toFixed(2) + ' €'}
                </CartTableCell>
                <TableCell>
                  <IconButton
                    aria-label="delete"
                    color="secondary"
                    onClick={() => {
                      setToDelete(item.itemId)
                    }}
                  >
                    <DeleteIcon fontSize="default" />
                  </IconButton>
                </TableCell>
              </TableRow>
            )
          })
        : null}
    </TableBody>
  )

  return (
    <Table size="small">
      {columnSizes}
      {tableHead}
      {tableBody}
    </Table>
  )
}

const CartTableCell: React.FC = ({ children }) => {
  const classes = useStyles()

  return (
    <TableCell align={'center'} className={classes.row}>
      {children}
    </TableCell>
  )
}

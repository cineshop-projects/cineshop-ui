import React from 'react'

import { Card, Grid, IconButton } from '@material-ui/core'
import RemoveIcon from '@material-ui/icons/Remove'
import AddIcon from '@material-ui/icons/Add'

import { CartItem, useUpdateCartItem } from '../../api/cineshop_api'
import { useAuth } from '../../providers/auth/AuthContext'

export default function ItemQuantity(props: {
  item: CartItem
  onQuantityUpdate: () => void
}) {
  const { item, onQuantityUpdate } = props
  const { authData } = useAuth()
  const { mutate: updateItemQuantity } = useUpdateCartItem({
    itemId: '' + item.itemId,
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })

  const incItemQuantity = (increment: 1 | -1) => {
    // TODO - add confirmation dialog if new value is 0
    updateItemQuantity({ quantity: item.quantity + increment })
      .then(() => {
        onQuantityUpdate()
      })
      .catch(() => {})
  }

  return (
    <Grid container spacing={0} alignItems={'center'}>
      <Grid container item sm={2} md={4} justify={'flex-end'}>
        <IconButton
          aria-label="dec"
          color="primary"
          onClick={() => {
            incItemQuantity(-1)
          }}
        >
          <RemoveIcon fontSize="default" />
        </IconButton>
      </Grid>
      <Grid item sm={8} md={4}>
        <Card variant={'outlined'}>{item.quantity}</Card>
      </Grid>
      <Grid container item sm={2} md={4} justify={'flex-start'}>
        <IconButton
          aria-label="inc"
          color="primary"
          onClick={() => {
            incItemQuantity(1)
          }}
        >
          <AddIcon fontSize="default" />
        </IconButton>
      </Grid>
    </Grid>
  )
}

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    padding: 10,
  },
  paper: {
    padding: 10,
  },
  action: {
    flexGrow: 1,
    alignItems: 'flex-end',
  },
  row: {
    fontSize: 14,
  },
  media: {
    height: 200,
  },
  description: {
    height: '100%',
    fontSize: 14,
  },
  summaryTitle: {
    fontWeight: 600,
    fontSize: 18,
  },
  total: {
    fontWeight: 600,
    fontSize: 20,
  },
}))

export default useStyles

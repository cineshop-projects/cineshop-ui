import React from 'react'

import { StaticContext } from 'react-router'
import { Redirect, RouteComponentProps } from 'react-router-dom'
import { makeStyles, Paper, Typography } from '@material-ui/core'
import { PaymentStatus } from '../../api/cineshop_api'

const useStyles = makeStyles(theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 800,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
}))

interface MatchParams {
  status: PaymentStatus
}

export default function PaymentConfirmation(
  props: RouteComponentProps<{}, StaticContext, MatchParams>
) {
  const { location } = props

  const classes = useStyles()

  let status: PaymentStatus | undefined = undefined
  if (location && location.state && location.state.status) {
    status = location.state.status
  }

  if (!status) {
    return <Redirect to={'/'} />
  }

  const accepted = status.status === 'APPROVE'

  return (
    <div className={classes.layout}>
      <Paper className={classes.paper}>
        {accepted ? (
          <>
            <Typography variant="h5" gutterBottom>
              Thank you for your order.
            </Typography>
            <Typography variant="subtitle1">
              Your order number is #{status.transactionId}.
            </Typography>
            <Typography variant="subtitle1">
              We have emailed your order confirmation, and will send you an update when
              your order has shipped.
            </Typography>
          </>
        ) : (
          <>
            <Typography variant="h5" gutterBottom>
              We had an error confirming your order.
            </Typography>
            <Typography variant="subtitle1">
              Your payment could not be processed. Please wait a few minutes before trying
              again or contact your bank.
            </Typography>
          </>
        )}
      </Paper>
    </div>
  )
}

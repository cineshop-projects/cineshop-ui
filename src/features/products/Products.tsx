import React, { useEffect, useState } from 'react'

import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core'
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart'
import { toast } from 'react-toastify'

import { Product, useAddCartItem, useGetProducts } from '../../api/cineshop_api'
import { useAuth } from '../../providers/auth/AuthContext'
import { isApiError, isDataError } from '../../api/error_utils'
import { useShared } from '../../providers/sharing/SharingContext'
import { getImage } from '../../config'

export default function Products() {
  const { authData } = useAuth()
  const { data: products } = useGetProducts({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })
  const { sharedData } = useShared()
  const [filteredProducts, setFilteredProducts] = useState<Product[]>([])

  useEffect(() => {
    if (products) {
      const filter = sharedData.filter.toLowerCase()
      if (!filter) {
        setFilteredProducts([...products])
        return
      }
      const newProducts: Product[] = []
      products.forEach(product => {
        if (product.label.toLowerCase().includes(filter)) {
          newProducts.push(product)
        }
      })
      setFilteredProducts(newProducts)
    }
  }, [products, sharedData])

  if (products) {
    return (
      <Box m={5}>
        <Grid container spacing={3}>
          {filteredProducts.map((product, index) => {
            return (
              <Grid item key={index} xs={6} md={4} lg={3}>
                <ProductCard product={product} />
              </Grid>
            )
          })}
        </Grid>
      </Box>
    )
  }

  return <div />
}

export function ProductCard(props: { product: Product }) {
  const { authData } = useAuth()
  const { onUpdate } = useShared()
  const { mutate: addItem } = useAddCartItem({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })

  const classes = makeStyles({
    media: {
      height: 300,
    },
    description: {
      height: '100%',
    },
    card: {
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },
    action: {
      flexGrow: 1,
      alignItems: 'flex-end',
    },
    price: {
      marginLeft: 5,
    },
  })()

  const {
    product: { id, label, description, image, price },
  } = props

  const onClick = () => {
    addItem({ productId: id })
      .then(() => {
        toast.success(`${label} was added to the cart`, { autoClose: 2000 })
      })
      .catch(err => {
        if (!err || !isDataError(err)) {
          toast.error(`Failed to add ${label} the cart: Unexpected error`)
          return
        }
        if (err.status && err.status < 300) {
          toast.success(`${label} was added to the cart`, { autoClose: 2000 })
          return
        }
        if (isApiError(err.data)) {
          toast.error(`Failed to add ${label} the cart: ${err.data.message}`)
        } else {
          toast.error(`Failed to add ${label} the cart: ${err.data}`)
        }
      })
      .finally(() => onUpdate())
  }

  const content = (
    <CardActionArea>
      <CardMedia
        className={classes.media}
        image={image ? image : getImage(id)}
        title={label}
      />
      <CardContent className={classes.description}>
        <Typography gutterBottom variant="h5" component="h2">
          {label}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {description}
        </Typography>
      </CardContent>
    </CardActionArea>
  )

  const action = (
    <CardActions className={classes.action}>
      <Grid container justify={'flex-start'} className={classes.price}>
        <Typography variant="h5" component="h2">
          {price.toFixed(2)} €
        </Typography>
      </Grid>
      <Grid container justify={'flex-end'}>
        <Button
          variant={'contained'}
          color="secondary"
          endIcon={<AddShoppingCartIcon />}
          onClick={onClick}
        >
          Add to cart
        </Button>
      </Grid>
    </CardActions>
  )

  return (
    <Card className={classes.card}>
      {content}
      {action}
    </Card>
  )
}

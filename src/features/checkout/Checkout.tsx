import React, { useEffect, useState } from 'react'

import { Paper, Step, StepLabel, Stepper, Typography } from '@material-ui/core'

import Review from './Review'
import PaymentForm from './PaymentForm'
import AddressForm from './AddressForm'
import { newAddress, newCreditCard } from './models'
import useStyles from './styles'
import {
  Address,
  CreditCard,
  PaymentStatus,
  useGetCart,
  useValidatePayment,
} from '../../api/cineshop_api'
import { useAuth } from '../../providers/auth/AuthContext'
import { toast } from 'react-toastify'
import { isApiError, isDataError } from '../../api/error_utils'
import { useHistory } from 'react-router-dom'

export default function Checkout() {
  const { authData } = useAuth()
  const { data: cart } = useGetCart({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })
  const { mutate: placeOrder } = useValidatePayment({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })

  const [activeStep, setActiveStep] = useState(0)
  const [shippingAddress, setShippingAddress] = useState(newAddress())
  const [useSameAddress, setUseSameAddress] = useState(false)
  const [billingAddress, setBillingAddress] = useState(newAddress())
  const [creditCard, setCreditCard] = useState(newCreditCard())
  const [paymentStatus, setPaymentStatus] = useState<PaymentStatus>()

  const history = useHistory()

  const classes = useStyles()

  useEffect(() => {
    if (paymentStatus) {
      history.replace('/payment', { status: paymentStatus })
    }
  }, [history, paymentStatus])

  const steps = [
    'Shipping address',
    'Billing address',
    'Payment details',
    'Review your order',
  ]

  const onNext = () => {
    if (activeStep === 0 && useSameAddress) {
      setActiveStep(activeStep + 2)
    } else {
      setActiveStep(activeStep + 1)
    }
  }

  const onBack = () => {
    if (activeStep === 2 && useSameAddress) {
      setActiveStep(activeStep - 2)
    } else {
      setActiveStep(activeStep - 1)
    }
  }

  const getStepContent = (step: number) => {
    if (step === 0 || step === 1) {
      return (
        <AddressForm
          title={steps[step]}
          value={step === 0 ? { ...shippingAddress } : { ...billingAddress }}
          step={step}
          useSameAddress={useSameAddress}
          onUseSameAddress={setUseSameAddress}
          onNext={(address: Address) => {
            if (step === 0 && useSameAddress) {
              setShippingAddress(address)
              setBillingAddress(address)
            } else {
              setBillingAddress(address)
            }
            onNext()
          }}
          onBack={step === 0 ? undefined : onBack}
        />
      )
    }

    if (step === 2) {
      return (
        <PaymentForm
          value={{ ...creditCard }}
          step={2}
          onNext={(creditCard: CreditCard) => {
            setCreditCard(creditCard)
            onNext()
          }}
          onBack={onBack}
        />
      )
    }

    if (step === 3 && cart) {
      return (
        <Review
          cart={cart}
          address={shippingAddress}
          creditCard={creditCard}
          step={3}
          onNext={() => {
            placeOrder({
              cartVersion: cart.lastUpdate,
              shippingAddress: shippingAddress,
              billingAddress: billingAddress,
              creditCard: creditCard,
            })
              .then(paymentStatus => {
                setPaymentStatus(paymentStatus)
              })
              .catch(err => {
                if (!err || !isDataError(err)) {
                  toast.error(`Failed to place order: Unexpected error`)
                  return
                }
                if (isApiError(err.data)) {
                  toast.error(`Failed to place order: ${err.data.message}`)
                } else {
                  toast.error(`Failed to place order: ${err.data}`)
                }
              })
          }}
          onBack={onBack}
        />
      )
    }

    return <div />
  }

  const title = (
    <Typography component="h1" variant="h4" align="center">
      Checkout
    </Typography>
  )

  const stepper = (
    <Stepper activeStep={activeStep} className={classes.stepper}>
      {steps.map(label => (
        <Step key={label}>
          <StepLabel>{label}</StepLabel>
        </Step>
      ))}
    </Stepper>
  )

  const content = (
    <>{activeStep === steps.length ? <div /> : <>{getStepContent(activeStep)}</>}</>
  )

  return (
    <div className={classes.layout}>
      <Paper className={classes.paper}>
        {title}
        {stepper}
        {content}
      </Paper>
    </div>
  )
}

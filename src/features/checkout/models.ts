import { Address, CreditCard } from '../../api/cineshop_api'

export function newAddress(): Address {
  return {
    firstName: '',
    lastName: '',
    address: '',
    addressComplement: '',
    city: '',
    state: '',
    postalCode: '',
    country: '',
  }
}

export function newCreditCard(): CreditCard {
  return {
    cardHolder: 'Ronald Dubinski',
    cardNumber: '4946518748218112',
    expiryDate: '04/25',
    cvv: '232',
  }
}

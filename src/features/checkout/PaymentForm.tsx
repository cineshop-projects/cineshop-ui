import React from 'react'

import { Grid, Typography } from '@material-ui/core'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'

import FormTextField from '../form-text-field/FormTextField'
import Buttons from './Buttons'
import { isValidCard } from './crediCardUtils'
import { CreditCard } from '../../api/cineshop_api'

export default function PaymentForm(props: {
  value: CreditCard
  step: number
  onNext: (creditCard: CreditCard) => void
  onBack: () => void
}) {
  const { value, step, onNext, onBack } = props

  const form = (
    <Formik
      initialValues={value}
      enableReinitialize
      onSubmit={values => {
        onNext(values)
      }}
      validationSchema={Yup.object().shape({
        cardHolder: Yup.string().required(
          'Full name of the Credit Card holder is required'
        ),
        cardNumber: Yup.string()
          .required('Credit Card number is required')
          .test('cardNumber-test', 'Credit Card number is invalid', value => {
            if (!value) {
              return false
            }
            return isValidCard(value)
          }),
        expiryDate: Yup.string()
          .required('Credit Card expiry date is required')
          .matches(
            /(0[1-9]|1[012])\/([0-9]{2})/,
            'Expiry date should be of the form MM/YY'
          )
          .test('expiryDate-test', 'Expiry date should be in the future', value => {
            if (!value) {
              return false
            }
            const [strMonth, strYear] = value.split('/')
            const month = +strMonth
            const year = +strYear
            if (isNaN(month) || isNaN(year)) {
              return false
            }
            const expiryDate = new Date()
            expiryDate.setFullYear(year + 2000, month, 1)

            return !(expiryDate < new Date())
          }),
        cvv: Yup.string()
          .min(3)
          .max(4)
          .required('Credit Card CVV is required')
          .matches(/([0-9]{3,4})/, 'CVV should be 3 to 4 digits'),
      })}
    >
      {props => {
        const { isSubmitting, submitForm } = props
        return (
          <Form>
            <Typography variant="h6" gutterBottom>
              Payment method
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={12} md={6}>
                    <FormTextField
                      id="cardHolder"
                      name="cardHolder"
                      label="Name on card"
                      size="medium"
                      variant="standard"
                      autoComplete="cc-name"
                      required
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <FormTextField
                      id="cardNumber"
                      name="cardNumber"
                      label="Card number"
                      size="medium"
                      variant="standard"
                      autoComplete="cc-number"
                      required
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <FormTextField
                      id="expiryDate"
                      name="expiryDate"
                      label="Expiry date"
                      placeholder="MM/YY"
                      size="medium"
                      variant="standard"
                      autoComplete="cc-exp"
                      required
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <FormTextField
                      id="cvv"
                      name="cvv"
                      label="CVV"
                      size="medium"
                      variant="standard"
                      helperText="Last three or four digits on signature strip"
                      autoComplete="cc-csc"
                      required
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Buttons
              activeStep={step}
              disabled={isSubmitting}
              onNext={() => {
                submitForm()
                  .then(() => {})
                  .catch(() => {})
              }}
              onBack={onBack}
            />
          </Form>
        )
      }}
    </Formik>
  )

  return <>{form}</>
}

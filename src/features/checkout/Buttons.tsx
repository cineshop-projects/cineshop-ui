import React from 'react'

import { Button } from '@material-ui/core'

import useStyles from './styles'

export default function Buttons(props: {
  activeStep: number
  isLastStep?: boolean
  disabled?: boolean
  onNext: () => void
  onBack?: () => void
}) {
  const { activeStep, isLastStep, disabled, onNext, onBack } = props

  const classes = useStyles()

  return (
    <div className={classes.buttons}>
      {activeStep !== 0 && (
        <Button onClick={onBack} className={classes.button}>
          Back
        </Button>
      )}
      <Button
        variant="contained"
        color="primary"
        onClick={onNext}
        disabled={disabled}
        className={classes.button}
      >
        {isLastStep ? 'Place order' : 'Next'}
      </Button>
    </div>
  )
}

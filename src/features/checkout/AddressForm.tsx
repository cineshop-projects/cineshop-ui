import React from 'react'

import { Checkbox, FormControlLabel, Grid, Typography } from '@material-ui/core'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'

import FormTextField from '../form-text-field/FormTextField'
import Buttons from './Buttons'
import { Address } from '../../api/cineshop_api'

export default function AddressForm(props: {
  title: string
  value: Address
  step: number
  useSameAddress?: boolean
  onUseSameAddress?: (useSameAddress: boolean) => void
  onNext: (address: Address) => void
  onBack?: () => void
}) {
  const { title, value, step, onUseSameAddress, useSameAddress, onNext, onBack } = props

  const form = (
    <Formik
      initialValues={value}
      enableReinitialize
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(false)
        onNext(values)
      }}
      validationSchema={Yup.object().shape({
        firstName: Yup.string(),
        lastName: Yup.string(),
        address: Yup.string(),
        addressComplement: Yup.string(),
        city: Yup.string(),
        state: Yup.string(),
        zip: Yup.number(),
        country: Yup.string(),
      })}
    >
      {props => {
        const { isSubmitting, submitForm } = props
        return (
          <Form>
            <Typography variant="h6" gutterBottom>
              {title}
            </Typography>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <FormTextField
                      id="firstName"
                      name="firstName"
                      label="First Name"
                      size="medium"
                      variant="standard"
                      autoComplete="given-name"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormTextField
                      id="lastName"
                      name="lastName"
                      label="Last Name"
                      size="medium"
                      variant="standard"
                      autoComplete="family-name"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <FormTextField
                      id="address"
                      name="address"
                      label="Address line 1"
                      size="medium"
                      variant="standard"
                      autoComplete="address-line1"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <FormTextField
                      id="addressComplement"
                      name="addressComplement"
                      label="Address line 2"
                      size="medium"
                      variant="standard"
                      autoComplete="address-line2"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormTextField
                      id="city"
                      name="city"
                      label="City"
                      size="medium"
                      variant="standard"
                      autoComplete="city"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormTextField
                      id="state"
                      name="state"
                      label="State/Province/Region"
                      size="medium"
                      variant="standard"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormTextField
                      id="postalCode"
                      name="postalCode"
                      label="Postal code/Zip"
                      size="medium"
                      variant="standard"
                      autoComplete="postal-code"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <FormTextField
                      id="country"
                      name="country"
                      label="Country"
                      size="medium"
                      variant="standard"
                      autoComplete="country"
                    />
                  </Grid>
                  {step === 0 ? (
                    <Grid item xs={12}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            color="secondary"
                            name="saveAddress"
                            value="yes"
                            checked={useSameAddress}
                            onChange={e => {
                              if (onUseSameAddress) {
                                onUseSameAddress(e.target.checked)
                              }
                            }}
                          />
                        }
                        label="Use this address for payment details"
                      />
                    </Grid>
                  ) : null}
                </Grid>
              </Grid>
            </Grid>
            <Buttons
              activeStep={step}
              disabled={isSubmitting}
              onNext={() => {
                submitForm()
                  .then(() => {})
                  .catch(() => {})
              }}
              onBack={step === 0 ? undefined : onBack}
            />
          </Form>
        )
      }}
    </Formik>
  )

  return <>{form}</>
}

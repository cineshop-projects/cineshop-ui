// https://gist.github.com/DiegoSalazar/4075533
// https://generator.creditcard/
// Takes a credit card string value and returns true on valid number
export function isValidCard(value: string) {
  if (!value) {
    return false
  }
  // Accept only digits, dashes or spaces
  if (/[^0-9-\s]+/.test(value)) return false

  // The Luhn Algorithm. It's so pretty.
  let nCheck = 0,
    bEven = false
  value = value.replace(/\D/g, '')

  for (let n = value.length - 1; n >= 0; n--) {
    let cDigit = value.charAt(n),
      nDigit = parseInt(cDigit, 10)

    if (bEven && (nDigit *= 2) > 9) nDigit -= 9

    nCheck += nDigit
    bEven = !bEven
  }

  return nCheck % 10 === 0
}

// https://stackoverflow.com/a/5911300
export function getCardType(number: string) {
  // visa
  let re = new RegExp('^4')
  if (number.match(re) != null) return 'Visa'

  // Mastercard
  // Updated for Mastercard 2017 BINs expansion
  if (
    /^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(
      number
    )
  )
    return 'Mastercard'

  // AMEX
  re = new RegExp('^3[47]')
  if (number.match(re) != null) return 'AMEX'

  // Discover
  re = new RegExp(
    '^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)'
  )
  if (number.match(re) != null) return 'Discover'

  // Diners
  re = new RegExp('^36')
  if (number.match(re) != null) return 'Diners'

  // Diners - Carte Blanche
  re = new RegExp('^30[0-5]')
  if (number.match(re) != null) return 'Diners - Carte Blanche'

  // JCB
  re = new RegExp('^35(2[89]|[3-8][0-9])')
  if (number.match(re) != null) return 'JCB'

  // Visa Electron
  re = new RegExp('^(4026|417500|4508|4844|491([37]))')
  if (number.match(re) != null) return 'Visa Electron'

  return ''
}

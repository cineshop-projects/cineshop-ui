import React from 'react'

import {
  Grid,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Typography,
} from '@material-ui/core'

import { Address, Cart, CreditCard } from '../../api/cineshop_api'
import { getCardType } from './crediCardUtils'
import Buttons from './Buttons'

const useStyles = makeStyles(theme => ({
  listItem: {
    padding: theme.spacing(1, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
}))

export default function Review(props: {
  cart: Cart
  address: Address
  creditCard: CreditCard
  step: number
  onNext: () => void
  onBack: () => void
}) {
  const { cart, address, creditCard, step, onNext, onBack } = props

  const classes = useStyles()

  const creditCardParts = [
    { name: 'Card type', detail: getCardType(creditCard.cardNumber) },
    { name: 'Card holder', detail: creditCard.cardHolder },
    { name: 'Card number', detail: creditCard.cardNumber },
    { name: 'Expiry date', detail: creditCard.expiryDate },
  ]

  return (
    <>
      <Typography variant="h6" gutterBottom>
        Order summary
      </Typography>
      <List disablePadding>
        {cart.items.map(item => (
          <ListItem className={classes.listItem} key={item.itemId}>
            <ListItemText primary={item.label} />
            <Typography variant="body2">
              {(item.price * item.quantity).toFixed(2) + ' €'}
            </Typography>
          </ListItem>
        ))}
        <hr />
        <ListItem className={classes.listItem}>
          <ListItemText primary="Subtotal" />
          <Typography variant="subtitle1" className={classes.total}>
            {cart.total.toFixed(2) + ' €'}
          </Typography>
        </ListItem>
        <ListItem className={classes.listItem}>
          <ListItemText primary="Discount" />
          <Typography variant="subtitle1" className={classes.total}>
            {(cart.total - cart.saleTotal).toFixed(2) + ' €'}
          </Typography>
        </ListItem>
        <ListItem className={classes.listItem}>
          <ListItemText primary="Total" />
          <Typography variant="subtitle1" className={classes.total}>
            {cart.saleTotal.toFixed(2) + ' €'}
          </Typography>
        </ListItem>
      </List>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Shipping
          </Typography>
          <Typography gutterBottom>
            {address.firstName + ' ' + address.lastName}
          </Typography>
          <Typography gutterBottom>{address.address}</Typography>
          <Typography gutterBottom>{address.addressComplement}</Typography>
          <Typography gutterBottom>
            {[address.city, address.postalCode]
              .filter(value => value && value.length > 0)
              .join(',')}
          </Typography>
          <Typography gutterBottom>
            {[address.state, address.country]
              .filter(value => value && value.length > 0)
              .join(',')}
          </Typography>
        </Grid>
        <Grid item container direction="column" xs={12} sm={6}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Payment details
          </Typography>
          <Grid container>
            {creditCardParts.map(creditCardInfo => (
              <React.Fragment key={creditCardInfo.name}>
                <Grid item xs={6}>
                  <Typography gutterBottom>{creditCardInfo.name}</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography gutterBottom>{creditCardInfo.detail}</Typography>
                </Grid>
              </React.Fragment>
            ))}
          </Grid>
        </Grid>
      </Grid>
      <Buttons activeStep={step} isLastStep onNext={onNext} onBack={onBack} />
    </>
  )
}

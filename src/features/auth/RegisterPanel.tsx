import React, { useEffect, useState } from 'react'

import { Link } from 'react-router-dom'
import { Button, Grid, Typography } from '@material-ui/core'
import { Form, Formik } from 'formik'
import { toast } from 'react-toastify'
import * as Yup from 'yup'

import { useAuth } from '../../providers/auth/AuthContext'
import { SignInResponse, useSignup } from '../../api/cineshop_api'
import { isApiError, isDataError } from '../../api/error_utils'
import FormTextField from '../form-text-field/FormTextField'
import useStyles from './styles'

export default function RegisterPanel() {
  const { onLogin } = useAuth()
  const { mutate: register } = useSignup({})
  const [response, setResponse] = useState<SignInResponse>()
  const classes = useStyles()

  useEffect(() => {
    if (response) {
      onLogin(response)
    }
  }, [onLogin, response])

  const form = (
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        passwordConfirmation: '',
      }}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(true)
        register({
          firstName: values.firstName,
          lastName: values.lastName,
          email: values.email,
          password: values.password,
        })
          .then(value => {
            setResponse(value)
          })
          .catch(err => {
            if (!err || !isDataError(err)) {
              toast.error(`Failed to sign in: Unexpected error`)
              return
            }
            if (isApiError(err.data)) {
              toast.error(`Failed to sign in: ${err.data.message}`)
            } else {
              toast.error(`Failed to sign in: ${err.data}`)
            }
          })
          .finally(() => {
            setSubmitting(false)
          })
      }}
      validationSchema={Yup.object().shape({
        firstName: Yup.string().required('First name is required'),
        lastName: Yup.string().required('Last name is required'),
        email: Yup.string().email().required('Email address is required'),
        password: Yup.string()
          .min(8, 'Password is too short, it should be at least 8 characters long')
          .required('Password is required'),
        passwordConfirmation: Yup.string()
          .oneOf([Yup.ref('password'), undefined], 'Passwords must match')
          .required('Password confirmation is required'),
      })}
    >
      {props => {
        const { isSubmitting } = props
        return (
          <Form className={classes.form}>
            <FormTextField id="firstName" name="firstName" label="First Name" required />
            <FormTextField id="lastName" name="lastName" label="Last Name" required />
            <FormTextField id="email" name="email" label="Email Address" required />
            <FormTextField
              id="password"
              name="password"
              label="Password"
              type="password"
              required
            />
            <FormTextField
              id="passwordConfirmation"
              name="passwordConfirmation"
              label="Confirm password"
              type="password"
              required
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={isSubmitting}
            >
              Sign Up
            </Button>
            <Grid container justify={'center'}>
              <Grid item>
                <Link to="/login">I already have an account, sign me in</Link>
              </Grid>
            </Grid>
          </Form>
        )
      }}
    </Formik>
  )

  return (
    <>
      <Typography component="h1" variant="h5">
        Sign up
      </Typography>
      {form}
    </>
  )
}

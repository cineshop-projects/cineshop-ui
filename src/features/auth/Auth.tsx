import React from 'react'

import { Avatar, CssBaseline, Grid, Paper } from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import { Redirect, Route, Switch } from 'react-router-dom'

import { useAuth } from '../../providers/auth/AuthContext'
import Copyright from '../copyright/Copyright'
import useStyles from './styles'
import LoginPanel from './LoginPanel'
import RegisterPanel from './RegisterPanel'

export default function Auth() {
  const { authData } = useAuth()

  return authData.authenticated ? <Redirect to={'/'} /> : <AuthPage />
}

function AuthPage() {
  const classes = useStyles()

  const authForm = (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <img
            src={'/static/images/logo.png'}
            alt={'Cineshop logo'}
            className={classes.brand}
          />
          <img
            src={'/static/images/name.png'}
            alt={'Cineshop brand name'}
            className={classes.brand}
          />
          <div style={{ height: 15 }} />
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Switch>
            <Route path="/login" component={LoginPanel} />
            <Route path="/register" component={RegisterPanel} />
          </Switch>
        </div>
        <Copyright />
      </Grid>
    </Grid>
  )

  return <>{authForm}</>
}

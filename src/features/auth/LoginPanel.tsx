import React, { useEffect, useState } from 'react'

import { Link } from 'react-router-dom'
import { Button, Grid, Typography } from '@material-ui/core'
import { Form, Formik } from 'formik'
import { toast } from 'react-toastify'
import * as Yup from 'yup'

import { useAuth } from '../../providers/auth/AuthContext'
import { SignInResponse, useSignin } from '../../api/cineshop_api'
import { isApiError, isDataError } from '../../api/error_utils'
import FormTextField from '../form-text-field/FormTextField'
import useStyles from './styles'

export default function LoginPanel() {
  const { onLogin } = useAuth()
  const { mutate: login } = useSignin({})
  const [response, setResponse] = useState<SignInResponse>()
  const classes = useStyles()

  useEffect(() => {
    if (response) {
      onLogin(response)
    }
  }, [onLogin, response])

  const form = (
    <Formik
      initialValues={{ email: '', password: '' }}
      onSubmit={(values, { setSubmitting }) => {
        setSubmitting(true)
        login({ email: values.email, password: values.password })
          .then(value => {
            setResponse(value)
          })
          .catch(err => {
            if (!err || !isDataError(err)) {
              toast.error(`Failed to sign in: Unexpected error`)
              return
            }
            if (isApiError(err.data)) {
              toast.error(`Failed to sign in: ${err.data.message}`)
            } else {
              toast.error(`Failed to sign in: ${err.data}`)
            }
          })
          .finally(() => {
            setSubmitting(false)
          })
      }}
      validationSchema={Yup.object().shape({
        email: Yup.string().email().required('Required'),
        password: Yup.string().required('Required'),
      })}
    >
      {props => {
        const { isSubmitting } = props
        return (
          <Form className={classes.form}>
            <FormTextField id="email" name="email" label="Email Address" required />
            <FormTextField
              id="password"
              name="password"
              label="Password"
              type="password"
              required
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={isSubmitting}
            >
              Sign In
            </Button>
            <Grid container justify={'center'}>
              <Grid item>
                <Link to="/register">I don't have an account yet, let me sign up</Link>
              </Grid>
            </Grid>
          </Form>
        )
      }}
    </Formik>
  )

  return (
    <>
      <Typography component="h1" variant="h5">
        Sign in
      </Typography>
      {form}
    </>
  )
}

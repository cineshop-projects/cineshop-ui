import React from 'react'

import Typography from '@material-ui/core/Typography'
import Link from '@material-ui/core/Link'

export default function Copyright() {
  return (
    <>
      <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link
          color="inherit"
          href="https://gitlab.com/cineshop-projects"
          target="_blank"
          rel="noopener"
        >
          Atef N.
        </Link>{' '}
        {new Date().getFullYear()}
      </Typography>
      <Typography variant="body2" color="textSecondary" align="center">
        {'Powered by '}
        <Link
          color="inherit"
          href="https://material-ui.com/"
          target="_blank"
          rel="noopener"
        >
          Material-UI
        </Link>
      </Typography>
    </>
  )
}

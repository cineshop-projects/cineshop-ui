import React, { useEffect, useState } from 'react'

import { useHistory } from 'react-router-dom'
import {
  AppBar,
  Badge,
  Box,
  Button,
  CardMedia,
  createStyles,
  fade,
  IconButton,
  InputBase,
  makeStyles,
  Theme,
  Toolbar,
} from '@material-ui/core'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'

import { useSignout } from '../../api/cineshop_api'
import { useAuth } from '../../providers/auth/AuthContext'
import SearchIcon from '@material-ui/icons/Search'
import { useShared } from '../../providers/sharing/SharingContext'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('lg')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('lg')]: {
        width: '100ch',
      },
    },
  })
)

export default function AppToolbar() {
  const { onUpdate } = useShared()
  const [redirect, setRedirect] = useState(false)
  const history = useHistory()
  const classes = useStyles()

  useEffect(() => {
    if (redirect) {
      onUpdate('')
      history.push('/products')
      setRedirect(false)
    }
  }, [history, onUpdate, redirect])

  const logo = (
    <Box my={0} mr={2}>
      <Button
        onClick={() => {
          setRedirect(true)
        }}
      >
        <CardMedia
          component="img"
          alt="Cineshop logo"
          height="40"
          src="/static/images/logo.png"
        />
      </Button>
    </Box>
  )

  const title = (
    <Box my={0}>
      <Button
        onClick={() => {
          setRedirect(true)
        }}
      >
        <CardMedia
          component="img"
          alt="Cineshop"
          height="40"
          src="/static/images/name.png"
        />
      </Button>
    </Box>
  )

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar variant="dense">
          {logo}
          {title}
          <div className={classes.grow} />
          <Search />
          <div className={classes.grow} />
          <CartBadge />
          <Logout />
        </Toolbar>
      </AppBar>
    </div>
  )
}

function Search() {
  const { onUpdate, sharedData } = useShared()
  const history = useHistory()

  const classes = useStyles()

  const onChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    onUpdate(e.target.value ? e.target.value : '')
    history.push('/products')
  }

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Search…"
        value={sharedData.filter}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
        onChange={onChange}
      />
    </div>
  )
}

function CartBadge() {
  const { sharedData, onUpdate } = useShared()
  const [count, setCount] = useState(0)
  const [redirect, setRedirect] = useState(false)
  const history = useHistory()

  const classes = useStyles()

  useEffect(() => {
    if (redirect) {
      history.push('/cart')
      setRedirect(false)
    }
  }, [history, redirect])

  useEffect(() => {
    setCount(sharedData.count)
  }, [sharedData])

  return (
    <IconButton
      className={classes.menuButton}
      color="inherit"
      aria-label="cart"
      onMouseEnter={() => onUpdate()}
      onClick={() => {
        setRedirect(true)
      }}
    >
      <Badge badgeContent={count} color="secondary">
        <ShoppingCartIcon />
      </Badge>
    </IconButton>
  )
}

function Logout() {
  const [redirect, setRedirect] = useState(false)
  const { authData, onLogout } = useAuth()
  const { mutate: logout } = useSignout({
    requestOptions: () => ({ headers: { Authorization: authData.accessToken } }),
  })

  const classes = useStyles()

  useEffect(() => {
    if (redirect) {
      onLogout()
      setRedirect(false)
    }
  }, [onLogout, redirect])

  const onClick = () => {
    logout()
      .then(() => {})
      .catch(() => {})
      .finally(() => {
        setRedirect(true)
      })
  }

  return (
    <IconButton
      className={classes.menuButton}
      color="inherit"
      aria-label="logout"
      onClick={onClick}
    >
      <ExitToAppIcon />
    </IconButton>
  )
}

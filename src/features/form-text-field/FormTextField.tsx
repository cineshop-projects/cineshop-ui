import React from 'react'

import { Field } from 'formik'
import { TextField } from 'formik-material-ui'

export default function FormTextField(props: any) {
  return (
    <Field
      component={TextField}
      variant="outlined"
      size="small"
      margin="normal"
      fullWidth
      {...props}
    />
  )
}
